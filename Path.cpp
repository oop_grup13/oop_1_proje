#include"Path.h"
using namespace std;
Path::Path()
{
	head = NULL;
	tail = NULL;
}
void Path::addPos(Pose pose)
{
	Node *newNode = new Node;
	newNode->pose = pose;
	newNode->next = NULL;

	if (head == NULL)
	{
		head = newNode;
		tail = newNode;
		newNode = NULL;
	}
	else
	{
		tail->next = newNode;
		tail = newNode;
	}
	number++;
}
void Path::print()
{
	Node *temp;
	temp = head;

	while (temp != nullptr)
	{
		cout << temp->pose.getX() << " ";
		cout << temp->pose.getY() << " ";
		cout << temp->pose.getTh() << endl;
		temp = temp->next;
	}
}
Pose Path::operator[](int index)
{
	Node *temp;
	temp = head;
	for (int i = 0; i < index; i++)
		temp = temp->next;

	return temp->pose;
}
Pose Path::getPos(int index)
{
	Node *temp;
	temp = head;
	for (int i = 0; i < index; i++)
		temp = temp->next;

	return temp->pose;
}
bool Path::removePos(int index)
{
	Node *temp = head, *prev = head;

	if (index > number)
	{
		return false;
	}
	else if (index == 0)
	{
		temp = head;
		delete head;
		head = temp->next;
		number--;
		return true;
	}
	else if(index < number-1 && temp->next!=NULL)
	{
		temp = head;
		for (int i = 0; i < index; i++)
		{
			prev = temp;
			temp = temp->next;
		}
		prev->next = temp->next;
		delete temp;
		number--;
		return true;
	}
	else if (index == number)
	{
		temp = head;
		for (int i = 0; i < index; i++)
		{
			prev = temp;
			temp = temp->next;
		}
		delete temp;
		number--;
		tail = prev;
		tail->next = NULL;
		return true;		
	}
}
bool Path::insertPos(int index, Pose pose)
{
	Node* prev = 0;
	Node* temp = head;
	Node* newNode = new Node;
	newNode->pose = pose;

	if (index > number)
	{
		return false;
	}
	else if (index == 0)
	{
		temp = head;
		head = newNode;
		newNode->next = temp;
		number++;
		return true;
	}
	else if (index <= number)
	{
		for (int i = 0; i < index; i++)
		{
			prev = temp;
			temp = temp->next;
		}
		prev->next = newNode;
		newNode->next = temp;
		number++;
		return true;
	}
	else
		return false;
}

void Path::operator<<(Path &path)
{
	path.print();
}

void Path::operator >> (Pose pose)
{
	Node *newNode = new Node;
	tail->next = newNode;
	tail = newNode;
	tail->pose = pose;
	number++;
}
