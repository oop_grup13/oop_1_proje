#include<iostream>
#include"RobotOperator.h"

using namespace std;

int main()
{
	cout << "Enter a password: ";
	int temp;
	cin >> temp;
	RobotOperator test(temp);
	cout << endl << endl;
	int code;
	int choice;
	while (1)
	{
		cout << "1 Connect, 2 Print informations, 3 Exit: ";
		cin >> choice;

		if (choice == 1)
		{
			cout << "Enter the password: ";
			cin >> code;
			if (test.checkAccessCode(code) == true)
			{
				cout << "Connected !" << endl << endl;
			}
			else
			{
				cout << "Wrong password !" << endl << endl;
			}
		}
		else if (choice == 2)
		{
			test.print();
		}
		else if (choice == 3)
		{
			break;
		}
	
	}

	system("pause");
}