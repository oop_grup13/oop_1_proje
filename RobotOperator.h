#pragma once
#include"Encryption.h"
#include<iostream>
using namespace std;
/**
* \brief Robotun ba�l� olup olmad���n� kontrol edecek ve gerekli bilgileri tutacak s�n�f
*/
class RobotOperator {
private :
	/**
	* \brief Robotun ismi
	*/
	string name;
	/**
	* \brief Robotun soyismi
	*/
	string surname;
	/**
	* \brief Robotu ba�layabilmek i�in gerekli �ifre
	*/
	unsigned int accessCode;
	/**
	* \brief Robotun ba�l� olup olmama durumu
	*/
	bool accessState;
	/**
	* \brief Encryption s�n�f�ndan gelen fonksiyon ile �ifreleme yapar
	*/
	int encryptCode(int);
	/**
	* \brief Encryption s�n�f�ndan gelen fonksiyon ile �ifre ��zer
	*/
	int decryptCode(int);
public:
	/**
	* \brief Robotun ba�l� olup olmama durumunu kontrol eder
	*/
	bool checkAccessCode(int);
	/**
	* \brief Robotun bilgilerini yazd�r�r
	*/
	void print();
	/**
	* \brief Robotu olu�turan yap�c� fonksiyon
	*/
	RobotOperator(int);
};