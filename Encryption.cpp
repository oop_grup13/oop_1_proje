#include"Encryption.h"
#include<iostream>
using namespace std;

int Encryption::encrypt(int x)
{
	int array[4], temp;
	array[0] = (x / 1000);
	array[1] = (x % 1000) / 100;
	array[2] = (x % 100) / 10;
	array[3] = (x % 10);

	for (int i = 0; i < 4; i++)
	{
		array[i] = (array[i] + 7) % 10;
	}

	temp = array[0];
	array[0] = array[2];
	array[2] = temp;
	temp = array[1];
	array[1] = array[3];
	array[3] = temp;

	int enc = 0;
	enc += array[3];
	enc += (array[2] * 10);
	enc += (array[1] * 100);
	enc += (array[0] * 1000);

	return enc;
}
int Encryption::decrypt(int y)
{
	int array2[4], temp;
	array2[0] = (y / 1000);
	array2[1] = (y % 1000) / 100;
	array2[2] = (y % 100) / 10;
	array2[3] = (y % 10);

	for (int i = 0; i < 4; i++)
	{
		array2[i] = ((array2[i] + 10) - 7) % 10;
	}
	temp = array2[0];
	array2[0] = array2[2];
	array2[2] = temp;
	temp = array2[1];
	array2[1] = array2[3];
	array2[3] = temp;

	int enc = 0;
	enc += array2[3];
	enc += (array2[2] * 10);
	enc += (array2[1] * 100);
	enc += (array2[0] * 1000);

	return enc;
}