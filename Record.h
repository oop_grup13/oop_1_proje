/**
* Record.h
*
*  Created on: 16 ARALIK 2018
*\author ANIL CAN OZDEMIR
*\brief Record header file
**/
#pragma once
#include<iostream>
#include<fstream>
#include<string>

using namespace std;
/**
* \brief Dosya i�lemleri yapan s�n�f
*/
class Record
{
private:
	/**
	* \brief Dosya i�lemleri yap�lacak dosyan�n ismi
	*/
	string fileName;
	/**
	* \brief Dosya i�lemleri yap�lacak dosya
	*/
	fstream file;
public:
	/**
	* \brief Dosyan�n a��k olup olmad���n� kontrol eder
	*/
	bool openFile();
	/**
	* \brief Dosyan�n kapal� olup olmad���n� kontrol eder
	*/
	bool closeFile();
	/**
	* \brief Dosya ismi atamas� yapar
	*/
	void setFileName(string name);
	/**
	* \brief Dosya dan sat�r okumas� yapar
	*/
	string readLine();
	/**
	* \brief Girilen string parametresini dosyaya yazd�r�r
	*/
	bool writeLine(string line);
	/**
	* \brief Girilen string parametresini dosyaya yazd�r�r
	*/
	void operator<<(string str);
	/**
	* \brief Dosya dan stringe veri aktar�m� yapar
	*/
	string operator >> (ifstream &file);
};