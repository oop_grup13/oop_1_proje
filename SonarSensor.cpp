#include "SonarSensor.h"



SonarSensor::SonarSensor(PioneerRobotAPI * robotAPI)
{
	robotAPI->getSonarRange(ranges);
}

SonarSensor::~SonarSensor()
{

}

void SonarSensor::uptadeSensor(float ranges[])
{
	for (int i = 0; i < size; i++)
		this->ranges[i] = ranges[i];
}

float SonarSensor::getMin() {
	int index;
	index = 0;
	float min = ranges[index];
	for (index = 1; index < size; index++) {
		if (ranges[index] < min) {
			min = ranges[index];
		}
	}
	for (index = 0; index < size; index++) {
		if (ranges[index] == min) {
			return index;
		}

	}

}

float SonarSensor::getMax()
{
	int index;
	index = 0;
	float max = ranges[index];
	for (index = 1; index < size; index++) {
		if (ranges[index] > max) {
			max = ranges[index];
		}
	}
	for (index = 0; index < size; index++) {
		if (ranges[index] == max) {
			return index;
		}

	}

}

float SonarSensor::getAngle(int index)
{
	index = index % 180;

	if (index == 15 || index == 0)
		return -90;
	else if (index >= 1 && index <= 6)
		return -70 + index * 20;
	else if (index == 7 || index == 8)
		return 90;
	else if (index >= 9 && index <= 11)
		return -50 + index * 20;
	else if (index == 12)
		return -170;
	else if (index == 13)
		return -150;
	else if (index == 14)
		return -130;
}

float SonarSensor::getRange(int index)
{
	return ranges[index];
}

float SonarSensor::operator[](int index)
{
	return this->getRange(index);
}
