#pragma once

/**
	* \brief Şifreleme yapan sınıf
	*/
class Encryption {
public:
/**
	* \brief Şifre oluşturan fonksiyon
	* \param int Şifre oluşturulacak 4 haneli integer değer
	*/
	int encrypt(int);
	/**
	* \brief Şifre çözen fonksiyon
	* \param int Şifre çözmek için gerekli 4 haneli integer değer
	*/
	int decrypt(int);
};