﻿#pragma once


/**
* \author Enes
*/

/**
* \brief Pose olusturan sinif
*/
class Pose
{
public:
	/**
	* \brief Constructor
	*/
	Pose();
	/**
	* \brief Constructor
	*/
	Pose(float _x);
	/**
	* \brief Constructor
	*/
	Pose(float _x, float _y);
	/**
	* \brief Constructor
	*/
	Pose(float _x, float _y, float _th);
	/**
	* \brief Destructor
	*/
	~Pose();
	/**
	* \brief Pose sınıfının x degerini geri dondurur
	*/
	float getX() const;
	/**
	* \brief Pose sınıfının x degerini atar
	*/
	void setX(float);
	/**
	* \brief Pose sınıfının y degerini geri dondurur
	*/
	float getY() const;
	/**
	* \brief Pose sınıfının y degerini atar
	*/
	void setY(float);
	/**
	* \brief Pose sınıfının aci(th) degerini geri dondurur
	*/
	float getTh() const;
	/**
	* \brief Pose sınıfının aci(th) degerini atar
	*/
	void setTh(float);
	/**
	* \brief Pose sınıfında == operatoru icin overload
	*/
	bool operator ==(const Pose& pos);
	/**
	* \brief Pose sınıfında + operatoru icin overload
	*/
	Pose operator +(const Pose& pos);
	/**
	* \brief Pose sınıfında - operatoru icin overload
	*/
	Pose operator -(const Pose& pos);
	/**
	* \brief Pose sınıfında += operatoru icin overload
	*/
	Pose& operator +=(const Pose& pos);
	/**
	* \brief Pose sınıfında -= operatoru icin overload
	*/
	Pose& operator -=(const Pose& pos);
	/**
	* \brief Pose sınıfında < operatoru icin overload
	*/
	bool operator <(const Pose& pos);
	/**
	* \brief Pose sınıfında > operatoru icin overload
	*/
	bool operator >(const Pose& pos);
	/**
	* \brief geriye Pose'u donen fonksiyon
	*/
	Pose getPose() const;
	/**
	* \brief Pose sınıfının degerlerini atayan fonksiyon
	*/
	void setPose(float _x, float _y, float _th);
	/**
	* \brief Iki pose arasi mesafeyi donduren fonksiyon
	*/
	float findDistanceTo(Pose pos) const;
	/**
	* \brief Iki pose arasi aciyi donduren fonksiyon
	*/
	float findAngleTo(Pose pos);

private:
	float x;
	float y;
	float th;
};