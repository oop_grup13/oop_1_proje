#pragma once
#include "PioneerRobotAPI.h"
#define size 16
/**
* \author Bora Kutay KESKINOGLU 
* \brief Sonar mesafe sensoru i�in veri tutan ve yoneten s�n�f.
*/
class SonarSensor {

private:
	/*
	* \brief Sonar mesafe sensoru icin degerleri float tan�mlanm�s bir dizide tutar.
	*/
	float ranges[size];
	PioneerRobotAPI* robotAPI;

public:
	/*
	* \brief Constructor
	*/
	SonarSensor(PioneerRobotAPI *robotAPI);
	/*
	* \brief Destructor
	*/
	~SonarSensor();
	/*
	* \brief Mesafe degerlerini parametreye girilen dizi ile gunceller.
	*/
	void uptadeSensor(float ranges[]);
	/*
	* \brief Mesafe degerleri arasindaki en kucuk sayiyi ve indeksi dondurur.
	*/
	float getMin();
	/*
	* \brief Mesafe degerleri arasindaki en buyuk sayiyi ve indeksi dondurur.
	*/
	float getMax();
	/*
	* \brief Indeksi verilen sensorun acisini hesaplar ve dondurur.
	*/
	float getAngle(int);
	/*
	* \brief Verilen indeksin mesafe degerini dondurur.
	*/
	float getRange(int);
	/*
	* \brief Verilen indeksin mesafe degerini dondurur.getRange() ile benzer islevi gorur.
	*/
	float operator[](int);
};