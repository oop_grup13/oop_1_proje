#include"Pose.h"
#include<iostream>

using namespace std;

int test()
{
	Pose pose, pose1, pose2(1, 2), pose3(1, 2), pose4, pose5, pose6(10, 10), pose7(5, 0), pose8(10, 0);
	pose.setPose(3, 4, 5);
	pose1 = pose.getPose();
	if (pose == pose1)
		cout << "Works" << endl;
	pose4 = pose2 + pose3;
	cout << pose4.getX() << " " << pose4.getY() << endl;
	pose5 = pose2 + pose3;
	cout << pose5.getX() << " " << pose4.getY() << endl;
	cout << (pose5 += pose4).getX() << " " << (pose5 += pose4).getY() << endl;
	cout << (pose5 -= pose4).getX() << " " << (pose5 -= pose4).getY() << endl;
	if (pose8 < pose6)
		cout << "Works" << endl;
	if (pose6 > pose8)
		cout << "Works" << endl;
	cout << pose7.findDistanceTo(pose8) << endl;
	cout << pose.findDistanceTo(pose6) << endl;
	cout << pose7.findAngleTo(pose8) << endl;
	cout << pose.findAngleTo(pose2) << endl;
	system("Pause");

	return 0;
}