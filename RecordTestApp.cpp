/**
* RecordTest.cpp
*
*  Created on: 16 ARALIK 2018
*\author ANIL CAN OZDEMIR
*\brief record test Application
**/
#include<iostream>
#include"Record.h"
using namespace std;

int main()
{


	Record record;
	if (record.openFile());		// openFile test
	cout << "openFile test success" << endl;	

	if (record.closeFile())		// closeFile test
		cout << "closeFile test success" << endl;

	record.setFileName("output.txt");	// setFileName test

	string readline = record.readLine();	// readline test
	cout << readline << endl;

	string writeline = "This is writeline";	// writeline test
	record.writeLine(writeline);

	string stringWrite = "this is some text";	// << operator testi
	record << stringWrite;

	ifstream file;
	file.open("input.txt");		// >> okutma i�lemi input.txt ad�nda bir dosyadan yap�ld�
	string line = record >> file;

	cout << line;
	system("pause");

}