#pragma once
#include<iostream>
#include"Node.h"
/**
* \brief Path olu�turan s�n�f
*/
class Path
{
private:
	/**
	* \brief Ba�l� listede sonuncuyu tutar
	*/
	Node *tail;	
	/**
	* \brief Ba�l� listede ba�� tutar
	*/
	Node *head;	
	/**
	* \brief Ba�l� listedeki eleman say�s�n� tutar
	*/
	int number;	
public:
	/**
	* \brief Path i�in yap�c� fonksiyon.
	*/
	Path();	
	/**
	* \brief Ba�l� listeye gidilen pose u ekler
	*/
	void addPos(Pose);	
	/**
	* \brief Ba�l� listedeki gidilen t�m pose lar� yazd�r�r
	*/
	void print();		
	/**
	* \brief Ba�l� listeden istenen konumdaki pose u d�nd�r�r
	* getPos ile ayn� i�i yapar.
	*/
	Pose operator[](int);	
	/**
	* \brief Ba�l� listeden istenen konumdaki pose u d�nd�r�r
	*/
	Pose getPos(int);		
	/**
	* \brief Ba�l� listeden istenen konumdaki pose u ��kart�r
	*/
	bool removePos(int);	
	/**
	* \brief Ba�l� listede istenen konuma verilen pose u ekler
	*/
	bool insertPos(int, Pose);	
	/**
	* \brief Ba�l� listedeki gidilen t�m pose lar� yazd�r�r
	* print fonksiyonu ile ayn� i�levi g�r�r.
	*/
	void operator <<(Path &);
	/**
	* \brief Ba�l� listedeki son konuma verilen Pose u ekler.
	*/
	void operator >> (Pose);


};