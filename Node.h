#pragma once
#include"Pose.h"
#pragma once

/**
* \brief D���m olu�turan s�n�f.
*/
class Node {
public:
	/**
	* \brief Sonraki d���m� tutar.
	*/
	Node *next;
	/**
	* \brief Pose olu�turur.
	*/
	Pose pose;
	/**
	* \brief S�n�f i�in yap�c� fonksiyon.
	*/
	Node();
};