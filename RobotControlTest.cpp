#include "PioneerRobotAPI.h"
#include"RobotControl.h"
#include <iostream>
using namespace std;

PioneerRobotAPI *robot;
float sonars[16];
float laserData[181];


int main() {

	robot = new PioneerRobotAPI;
	RobotControl control(robot);
	if (!robot->connect()) {
		cout << "Could not connect..." << endl;
		return 0;
	}

	control.forward(1000);
	control.print(sonars,laserData);

	control.turnLeft();
	Sleep(1000);
	control.print(sonars, laserData);

	control.forward(1000);
	control.print(sonars, laserData);

	control.stopTurn();
	Sleep(1000);
	control.print(sonars, laserData);

	control.turnRight();
	Sleep(1000);
	control.print(sonars, laserData);

	control.backward(5000);
	Sleep(1000);

	control.stopmove();
	Pose X(100,200,30);
	control.setPose(X);
	control.print(sonars, laserData);
	Sleep(1000);
	control.print(sonars, laserData);

	cout << "Press any key to exit...";
	getchar();

	robot->disconnect();
	delete robot;
	return 0;

}