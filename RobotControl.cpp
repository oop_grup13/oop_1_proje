/**
* RobotControl.cpp
*
*  Created on: 16 ARALIK 2018
*\author ANIL CAN OZDEMIR
*\brief RobotControl.cpp
**/


#include "RobotControl.h"
/**
*\brief RobotControl.cpp
*\param robot:PioneerRobotAPI
**/

RobotControl::RobotControl(PioneerRobotAPI *robot)
{
	robotAPI = robot;
	float x, y, th;
	position = new Pose;
	x = robot->getX();
	y = robot->getY();
	th = robot->getTh();
	position->setX(x);
	position->setY(y);
	position->setTh(th);
}
/**
*\brief default--const
*
**/
RobotControl::~RobotControl()
{
	//constructor
}
/**
*\brief robotun sola d�nmesini sa�layan fonksiyon..
*
**/
void RobotControl::turnLeft()
{
	robotAPI->turnRobot(robotAPI->left);
}
/**
*\brief robotun sa�a d�nmesini sa�layan fonksiyon..
*
**/
void RobotControl::turnRight()
{
	robotAPI->turnRobot(robotAPI->right);
}
/**
*\brief robotun belli bir h�zda ilerlemesini sa�lar.
*
**/
void RobotControl::forward(float speed)
{
	robotAPI->moveRobot(speed);
}
/**
*\brief robotun pozisyonunu ve sonar-laser rangelerini ekrana doker.
*
**/
void RobotControl::print(float sonars[],float laserData[])
{
	{
		cout << "MyPose is (" << robotAPI->getX() << "," << robotAPI->getY() << "," << robotAPI->getTh() << ")" << endl;
		cout << "Sonar ranges are [ ";
		robotAPI->getSonarRange(sonars);
		for (int i = 0; i < 16; i++) {
			cout << sonars[i] << " ";
		}
		cout << "]" << endl;
		cout << "Laser ranges are [ ";
		robotAPI->getLaserRange(laserData);
		for (int i = 0; i < 181; i++) {
			cout << laserData[i] << " ";
		}
		cout << "]" << endl;
	}
}
/**
*\brief robotun belli bir h�zda geriye do�ru hareketini sa�lar.
*
**/
void RobotControl::backward(float speed)
{
	robotAPI->moveRobot(-speed);
}
/**
*\brief robotun pozisyonunu al�r.
*
**/
Pose RobotControl::getPose()
{
	Pose x(robotAPI->getX(),robotAPI->getY(),robotAPI->getTh());

	position=&x;
	return x;
}
/**
*\brief robotun pozisyonunu yeniden ayarlar.
*
**/
void RobotControl::setPose(Pose yeni)
{
	position = &yeni;
	robotAPI->setPose(yeni.getX(),yeni.getY(),yeni.getTh());
}
/**
*\brief robotun donusunu durdurur.
*
**/
void RobotControl::stopTurn()
{
	robotAPI->turnRobot(robotAPI->forward);
}
/**
*\brief robotun hareletini durdurur..
*
**/
void RobotControl::stopmove()
{
	robotAPI->stopRobot();
}
