/**
* RobotControl.h
*
*  Created on: 16 ARALIK 2018
*\author ANIL CAN OZDEMIR
*\brief RobotControl.h
**/
#pragma once
#include<iostream>
#include"Pose.h"
#include"PioneerRobotAPI.h"

using namespace std;

class RobotControl {
private:
	Pose* position;
	PioneerRobotAPI* robotAPI;
	int state;

public:
	RobotControl(PioneerRobotAPI*);
	~RobotControl();
	void turnLeft();
	void turnRight();
	void forward(float speed);
	void print(float [],float[]);
	void backward(float speed);
	Pose getPose();
	void setPose(Pose);
	void stopTurn();
	void stopmove();
};